import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [],
  imports: [CommonModule, MatToolbarModule, NgbModule, MatTableModule],
  exports: [MatToolbarModule, NgbModule, MatTableModule],
})
export class SharedModule {}
