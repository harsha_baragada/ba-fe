import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LandingComponent } from './landing/landing.component';
import { NgbdCarouselConfigComponent } from './landing/components/ngbd-carousel-config/ngbd-carousel-config.component';
import { SharedModule } from '../shared/shared.module';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { MissionComponent } from './mission/mission.component';

@NgModule({
  declarations: [LandingComponent, NgbdCarouselConfigComponent, ContactComponent, AboutComponent, MissionComponent],
  imports: [CommonModule, HomeRoutingModule, SharedModule],
})
export class HomeModule {}
